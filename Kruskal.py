import time


Gd = {
"a": {"b": 5, "c": 3},
"b": {"a": 5, "c": 7, "d": 9, "e": 6},
"c": {"a": 3, "b": 7, "f": 5},
"d": {"b": 9, "e": 7, "g": 8},
"e": {"b": 6, "d": 7, "f": 4},
"f": {"c": 5, "e": 4},
"g": {"d": 8}
}

Gt = [('a', 'b', 5), ('a', 'c', 3), ('b', 'c', 7), ('b', 'd', 9), ('b', 'e', 6), ('c', 'f', 5), ('d', 'e', 7), ('d', 'g', 8), ('e', 'f', 4)]

GLD = {'a': {'b': 4, 'bq': 2, 'cd': 2, 'cx': 5}, 'b': {'a': 4, 'c': 1, 'aq': 1, 'bx': 4}, 'bq': {'a': 2, 'ao': 4, 'at': 3, 'bp': 3, 'br': 2}, 'cd': {'a': 2, 't': 2, 'af': 5, 'ap': 6, 'bd': 8, 'cc': 2, 'ce': 4}, 'cx': {'a': 5, 'be': 4, 'bo': 2, 'ch': 1, 'cm': 3, 'cw': 3}, 'c': {'b': 1, 'd': 2, 'ah': 1, 'aw': 4, 'cl': 2}, 'aq': {'b': 1, 'o': 2, 'u': 4, 'ap': 3, 'ar': 2, 'au': 3}, 'bx': {'b': 4, 'g': 2, 'af': 3, 'aj': 3, 'bd': 2, 'bw': 3, 'by': 3}, 'd': {'c': 2, 'e': 2, 'v': 5, 'ah': 2, 'cg': 3, 'cj': 2, 'cm': 3}, 'ah': {'c': 1, 'd': 2, 'ag': 4, 'ai': 9, 'ax': 5}, 'aw': {'c': 4, 'm': 3, 'am': 1, 'av': 4, 'ax': 1, 'bn': 4, 'cs': 2}, 'cl': {'c': 2, 'bk': 3, 'bo': 5, 'ck': 2, 'cm': 4}, 'e': {'d': 2, 'f': 3, 'ac': 2, 'ad': 4, 'bi': 2, 'br': 2}, 'v': {'d': 5, 'k': 2, 'p': 2, 'u': 4, 'w': 4, 'ac': 5, 'ai': 4, 'bn': 4, 'cc': 2}, 'cg': {'d': 3, 'g': 1, 'j': 5, 'q': 2, 'aj': 2, 'av': 3, 'cf': 6, 'ch': 2, 'cv': 4}, 'cj': {'d': 2, 'm': 3, 'ar': 2, 'ci': 2, 'ck': 4}, 'cm': {'d': 3, 'ad': 6, 'cl': 4, 'cn': 4, 'cx': 3}, 'f': {'e': 3, 'g': 4}, 'ac': {'e': 2, 'h': 3, 'v': 5, 'ab': 2, 'ad': 2, 'ar': 3, 'co': 3, 'cv': 1}, 'ad': {'e': 4, 'ac': 2, 'ae': 4, 'ao': 4, 'cm': 6}, 'bi': {'e': 2, 'z': 4, 'ak': 3, 'ap': 6, 'bh': 4, 'bj': 1, 'bp': 2, 'ch': 2}, 'br': {'e': 2, 'k': 3, 'ag': 3, 'aj': 6, 'am': 3, 'bq': 2, 'bs': 5, 'cp': 3}, 'g': {'f': 4, 'h': 3, 'az': 3, 'bx': 2, 'cc': 1, 'cg': 1, 'ci': 3}, 'h': {'g': 3, 'i': 4, 'ac': 3, 'ap': 4, 'ca': 4}, 'az': {'g': 3, 'al': 3, 'ba': 3, 'bg': 1, 'cb': 1, 'cv': 5}, 'cc': {'g': 1, 'v': 2, 'bm': 3, 'cb': 4, 'cd': 2}, 'ci': {'g': 3, 'bc': 5, 'bn': 4, 'ch': 5, 'cj': 2}, 'i': {'h': 4, 'j': 6, 't': 3, 'ap': 1, 'bd': 1}, 'ap': {'h': 4, 'i': 1, 'ab': 7, 'ao': 3, 'aq': 3, 'bg': 3, 'bi': 6, 'cd': 6, 'cs': 2}, 'ca': {'h': 4, 'bz': 2, 'cb': 2}, 'j': {'i': 6, 'k': 2, 'cg': 5}, 't': {'i': 3, 's': 2, 'u': 3, 'cd': 2}, 'bd': {'i': 1, 'bc': 3, 'be': 7, 'bx': 2, 'cd': 8}, 'k': {'j': 2, 'l': 2, 's': 4, 'v': 2, 'z': 6, 'ai': 5, 'an': 5, 'br': 3, 'cv': 3}, 'l': {'k': 2, 'm': 2}, 's': {'k': 4, 'r': 3, 't': 2, 'ab': 5, 'bm': 5}, 'z': {'k': 6, 'y': 4, 'aa': 2, 'at': 4, 'bg': 2, 'bi': 4}, 'ai': {'k': 5, 'v': 4, 'ah': 9, 'aj': 4, 'am': 6}, 'an': {'k': 5, 'm': 2, 'am': 3, 'ao': 4, 'bw': 3, 'cb': 3, 'cp': 1}, 'cv': {'k': 3, 'p': 5, 'ac': 1, 'az': 5, 'cg': 4, 'cu': 2, 'cw': 3}, 'm': {'l': 2, 'n': 1, 'w': 4, 'an': 2, 'aw': 3, 'bf': 2, 'bg': 5, 'cj': 3, 'co': 5, 'cq': 2}, 'n': {'m': 1, 'o': 2, 'w': 2}, 'w': {'m': 4, 'n': 2, 'v': 4, 'x': 2}, 'bf': {'m': 2, 'be': 1, 'bg': 2}, 'bg': {'m': 5, 'z': 2, 'ab': 6, 'ap': 3, 'at': 3, 'az': 1, 'bf': 2, 'bh': 2, 'bu': 4, 'ck': 3}, 'co': {'m': 5, 'o': 2, 'ac': 3, 'bb': 5, 'cp': 3}, 'cq': {'m': 2, 'u': 4, 'bt': 2, 'cp': 3, 'cr': 1}, 'o': {'n': 2, 'p': 1, 'aq': 2, 'co': 2}, 'p': {'o': 1, 'q': 5, 'v': 2, 'ck': 3, 'cv': 5}, 'q': {'p': 5, 'r': 4, 'cg': 2}, 'ck': {'p': 3, 'u': 4, 'bg': 3, 'cj': 4, 'cl': 2}, 'r': {'q': 4, 's': 3, 'ar': 3, 'cp': 4}, 'ar': {'r': 3, 'x': 4, 'ac': 3, 'aq': 2, 'as': 1, 'ay': 2, 'bt': 3, 'by': 5, 'cj': 2, 'cp': 3, 'at': 2}, 'cp': {'r': 4, 'an': 1, 'ar': 3, 'br': 3, 'bw': 5, 'co': 3, 'cq': 3}, 'ab': {'s': 5, 'aa': 2, 'ac': 2, 'ap': 7, 'ax': 2, 'bg': 6}, 'bm': {'s': 5, 'av': 4, 'bl': 4, 'bn': 2, 'bp': 5, 'cc': 3}, 'u': {'t': 3, 'v': 4, 'aq': 4, 'bc': 2, 'bz': 5, 'ck': 4, 'cq': 4}, 'bc': {'u': 2, 'bb': 4, 'bd': 3, 'bh': 2, 'ci': 5}, 'bz': {'u': 5, 'aa': 6, 'by': 3, 'ca': 2}, 'bn': {'v': 4, 'aw': 4, 'bm': 2, 'bo': 3, 'ci': 4}, 'x': {'w': 2, 'y': 4, 'ar': 4, 'bk': 3, 'bp': 4}, 'y': {'x': 4, 'z': 4, 'ae': 9}, 'bk': {'x': 3, 'bj': 4, 'bl': 1, 'cl': 3}, 'bp': {'x': 4, 'bi': 2, 'bm': 5, 'bo': 2, 'bq': 3}, 'ae': {'y': 9, 'ad': 4, 'af': 2, 'by': 3}, 'aa': {'z': 2, 'ab': 2, 'bz': 6}, 'at': {'z': 4, 'ar': 2, 'au': 2, 'bg': 3, 'bq': 3}, 'ax': {'ab': 2, 'ah': 5, 'ao': 6, 'aw': 1, 'ay': 4, 'bv': 4}, 'ao': {'ad': 4, 'an': 4, 'ap': 3, 'as': 1, 'ax': 6, 'bq': 4}, 'af': {'ae': 2, 'ag': 1, 'am': 5, 'bx': 3, 'by': 6, 'cd': 5}, 'by': {'ae': 3, 'af': 6, 'ar': 5, 'ba': 6, 'bx': 3, 'bz': 3}, 'ag': {'af': 1, 'ah': 4, 'br': 3}, 'am': {'af': 5, 'ai': 6, 'al': 5, 'an': 3, 'aw': 1, 'br': 3}, 'aj': {'ai': 4, 'ak': 5, 'br': 6, 'bx': 3, 'cg': 2}, 'ak': {'aj': 5, 'al': 3, 'bi': 3}, 'al': {'ak': 3, 'am': 5, 'ay': 4, 'az': 3}, 'ay': {'al': 4, 'ar': 2, 'ax': 4, 'ay': 2}, 'bw': {'an': 3, 'bu': 7, 'bv': 4, 'bx': 3, 'cp': 5}, 'cb': {'an': 3, 'az': 1, 'ca': 2, 'cc': 4, 'cn': 6}, 'as': {'ao': 1, 'ar': 1}, 'cs': {'ap': 2, 'aw': 2, 'ct': 4}, 'au': {'aq': 3, 'at': 2, 'au': 3}, 'bt': {'ar': 3, 'bs': 3, 'bu': 3, 'cq': 2}, 'av': {'aw': 4, 'bm': 4, 'ce': 5, 'cg': 3}, 'ce': {'av': 5, 'cd': 4, 'cf': 2}, 'bv': {'ax': 4, 'bu': 1, 'bw': 4}, 'ba': {'az': 3, 'bb': 3, 'bh': 2, 'by': 6}, 'bb': {'ba': 3, 'bc': 4, 'co': 5}, 'bh': {'ba': 2, 'bc': 2, 'bg': 2, 'bi': 4}, 'be': {'bd': 7, 'bf': 1, 'cx': 4}, 'bu': {'bg': 4, 'bt': 3, 'bv': 1, 'bw': 7}, 'bj': {'bi': 1, 'bk': 4}, 'ch': {'bi': 2, 'cg': 2, 'ci': 5, 'cx': 1}, 'bl': {'bk': 1, 'bm': 4, 'cn': 8}, 'cn': {'bl': 8, 'cb': 6, 'cm': 4, 'cn': 7}, 'bo': {'bn': 3, 'bp': 2, 'cl': 5, 'cx': 2}, 'bs': {'br': 5, 'bt': 3}, 'cf': {'ce': 2, 'cg': 6}, 'cr': {'cq': 1}, 'ct': {'cs': 4}, 'cu': {'cv': 2}, 'cw': {'cv': 3, 'cx': 3}}

GLT = [('a', 'b', 4), ('a', 'bq', 2), ('a', 'cd', 2), ('a', 'cx', 5), ('b', 'c', 1), ('b', 'aq', 1), ('b', 'bx', 4),
('c', 'd', 2), ('c', 'ah', 1), ('c', 'aw', 4), ('c', 'cl', 2), ('d', 'e', 2), ('d', 'v', 5), ('d', 'ah', 2),
('d', 'cg', 3), ('d', 'cj', 2), ('d', 'cm', 3), ('e', 'f', 3), ('e', 'ac', 2), ('e', 'ad', 4), ('e', 'bi', 2),
('e', 'br', 2), ('f', 'g', 4), ('g', 'h', 3), ('g', 'az', 3), ('g', 'bx', 2), ('g', 'cc', 1), ('g', 'cg', 1),
('g', 'ci', 3), ('h', 'i', 4), ('h', 'ac', 3), ('h', 'ap', 4), ('h', 'ca', 4), ('i', 'j', 6), ('i', 't', 3),
('i', 'ap', 1), ('i', 'bd', 1), ('j', 'k', 2), ('j', 'cg', 5), ('k', 'l', 2), ('k', 's', 4), ('k', 'v', 2),
('k', 'z', 6), ('k', 'ai', 5), ('k', 'an', 5), ('k', 'br', 3), ('k', 'cv', 3), ('l', 'm', 2), ('m', 'n', 1),
('m', 'w', 4), ('m', 'an', 2), ('m', 'aw', 3), ('m', 'bf', 2), ('m', 'bg', 5), ('m', 'cj', 3), ('m', 'co', 5),
('m', 'cq', 2), ('n', 'o', 2), ('n', 'w', 2), ('o', 'p', 1), ('o', 'aq', 2), ('o', 'co', 2), ('p', 'q', 5),
('p', 'v', 2), ('p', 'ck', 3), ('p', 'cv', 5), ('q', 'r', 4), ('q', 'cg', 2), ('r', 's', 3), ('r', 'ar', 3),
('r', 'cp', 4), ('s', 't', 2), ('s', 'ab', 5), ('s', 'bm', 5), ('t', 'u', 3), ('t', 'cd', 2), ('u', 'v', 4),
('u', 'aq', 4), ('u', 'bc', 2), ('u', 'bz', 5), ('u', 'ck', 4), ('u', 'cq', 4), ('v', 'w', 4), ('v', 'ac', 5),
('v', 'ai', 4), ('v', 'bn', 4), ('v', 'cc', 2), ('w', 'x', 2), ('x', 'y', 4), ('x', 'ar', 4), ('x', 'bk', 3),
('x', 'bp', 4), ('y', 'z', 4), ('y', 'ae', 9), ('z', 'aa', 2), ('z', 'at', 4), ('z', 'bg', 2), ('z', 'bi', 4),
('aa', 'ab', 2), ('aa', 'bz', 6), ('ab', 'ac', 2), ('ab', 'ap', 7), ('ab', 'ax', 2), ('ab', 'bg', 6), ('ac', 'ad', 2),
('ac', 'ar', 3), ('ac', 'co', 3), ('ac', 'cv', 1), ('ad', 'ae', 4), ('ad', 'ao', 4), ('ad', 'cm', 6), ('ae', 'af', 2),
('ae', 'by', 3), ('af', 'ag', 1), ('af', 'am', 5), ('af', 'bx', 3), ('af', 'by', 6), ('af', 'cd', 5), ('ag', 'ah', 4),
('ag', 'br', 3), ('ah', 'ai', 9), ('ah', 'ax', 5), ('ai', 'aj', 4), ('ai', 'am', 6), ('aj', 'ak', 5), ('aj', 'br', 6),
('aj', 'bx', 3), ('aj', 'cg', 2), ('ak', 'al', 3), ('ak', 'bi', 3), ('al', 'am', 5), ('al', 'ay', 4), ('al', 'az', 3),
('am', 'an', 3), ('am', 'aw', 1), ('am', 'br', 3), ('an', 'ao', 4), ('an', 'bw', 3), ('an', 'cb', 3), ('an', 'cp', 1),
('ao', 'ap', 3), ('ao', 'as', 1), ('ao', 'ax', 6), ('ao', 'bq', 4), ('ap', 'aq', 3), ('ap', 'bg', 3), ('ap', 'bi', 6),
('ap', 'cd', 6), ('ap', 'cs', 2), ('aq', 'ar', 2), ('aq', 'au', 3), ('ar', 'as', 1), ('ar', 'ay', 2), ('ar', 'bt', 3),
('ar', 'by', 5), ('ar', 'cj', 2), ('ar', 'cp', 3), ('ar', 'at', 2), ('at', 'au', 2), ('at', 'bg', 3), ('at', 'bq', 3),
('au', 'au', 3), ('av', 'aw', 4), ('av', 'bm', 4), ('av', 'ce', 5), ('av', 'cg', 3), ('aw', 'ax', 1), ('aw', 'bn', 4),
('aw', 'cs', 2), ('ax', 'ay', 4), ('ax', 'bv', 4), ('ay', 'ay', 2), ('az', 'ba', 3), ('az', 'bg', 1), ('az', 'cb', 1),
('az', 'cv', 5), ('ba', 'bb', 3), ('ba', 'bh', 2), ('ba', 'by', 6), ('bb', 'bc', 4), ('bb', 'co', 5), ('bc', 'bd', 3),
('bc', 'bh', 2), ('bc', 'ci', 5), ('bd', 'be', 7), ('bd', 'bx', 2), ('bd', 'cd', 8), ('be', 'bf', 1), ('be', 'cx', 4),
('bf', 'bg', 2), ('bg', 'bh', 2), ('bg', 'bu', 4), ('bg', 'ck', 3), ('bh', 'bi', 4), ('bi', 'bj', 1), ('bi', 'bp', 2),
('bi', 'ch', 2), ('bj', 'bk', 4), ('bk', 'bl', 1), ('bk', 'cl', 3), ('bl', 'bm', 4), ('bl', 'cn', 8), ('bm', 'bn', 2),
('bm', 'bp', 5), ('bm', 'cc', 3), ('bn', 'bo', 3), ('bn', 'ci', 4), ('bo', 'bp', 2), ('bo', 'cl', 5), ('bo', 'cx', 2),
('bp', 'bq', 3), ('bq', 'br', 2), ('br', 'bs', 5), ('br', 'cp', 3), ('bs', 'bt', 3), ('bt', 'bu', 3), ('bt', 'cq', 2),
('bu', 'bv', 1), ('bu', 'bw', 7), ('bv', 'bw', 4), ('bw', 'bx', 3), ('bw', 'cp', 5), ('bx', 'by', 3), ('by', 'bz', 3),
('bz', 'ca', 2), ('ca', 'cb', 2), ('cb', 'cc', 4), ('cb', 'cn', 6), ('cc', 'cd', 2), ('cd', 'ce', 4), ('ce', 'cf', 2),
('cf', 'cg', 6), ('cg', 'ch', 2), ('cg', 'cv', 4), ('ch', 'ci', 5), ('ch', 'cx', 1), ('ci', 'cj', 2), ('cj', 'ck', 4),
('ck', 'cl', 2), ('cl', 'cm', 4), ('cm', 'cn', 4), ('cm', 'cx', 3), ('cn', 'cn', 7), ('co', 'cp', 3), ('cp', 'cq', 3),
('cq', 'cr', 1), ('ct', 'cs', 4), ('cu', 'cv', 2), ('cv', 'cw', 3), ('cw', 'cx', 3)]

def to_tuple(graph):
    visited = []
    edges = []
    for node1, node1_edges in graph.items():
        visited.append(node1)
        for node2, weight in node1_edges.items():
            if node2 in visited:
                continue
            edges.append((node1,node2,weight))
    return edges


# print(to_tuple(G))


def kruskal(G):
    #set list of nodes
    nodes = list(map(lambda x: x[0], G))
    nodes += list(map(lambda x: x[1], G))
    nodes = set(nodes)
    #sort edges in ascending order
    g = sorted(G, key=lambda x: x[2])
    #initialise empty array for edges in MST
    F = []
    #initialise empty array for sets of nodes
    sets = []
    #for every node, add to unique set in sets[]
    for n in nodes:
        sets.append([n])
    #iterate through edges
    for e in g:
        u,v,w = e
        t = None
        s1 = None
        for s in sets[:]:
            if t is not None:
                if t in s:
                    F.append(e)
                    s.extend(s1)
                    break
                continue
            elif u in s and v in s:
                continue
            if u in s:
                t = v
            elif v in s:
                t = u
            else:
                continue
            sets.remove(s)
            s1 = s
    return F


def kruskal_dict(G):
    g = to_tuple(G)
    #set list of nodes
    nodes = list(map(lambda x: x[0], g))
    nodes += list(map(lambda x: x[1], g))
    nodes = set(nodes)
    #sort edges in ascending order
    g = sorted(g, key=lambda x: x[2])
    #initialise empty array for edges in MST
    F = []
    #initialise empty array for sets of nodes
    sets = []
    #for every node, add to unique set in sets[]
    for n in nodes:
        sets.append([n])
    #iterate through edges
    for e in g:
        u,v,w = e
        t = None
        s1 = None
        for s in sets[:]:
            if t is not None:
                if t in s:
                    F.append(e)
                    s.extend(s1)
                    break
                continue
            elif u in s and v in s:
                continue
            if u in s:
                t = v
            elif v in s:
                t = u
            else:
                continue
            sets.remove(s)
            s1 = s
    return F



def n_times(n):
    st = time.time()
    for x in range(0, n):
        kruskal(Gt)
        x+=1
    ft = time.time()
    return ft - st

def n_times_(n):
    st = time.time()
    g = to_tuple(Gd)
    for x in range(0, n):
        kruskal(g)
        x+=1
    ft = time.time()
    return ft - st

def n_timesl(n):
    st = time.time()
    for x in range(0, n):
        kruskal(GLT)
        x+=1
    ft = time.time()
    return ft - st

def n_times_l(n):
    st = time.time()
    g = to_tuple(GLD)
    for x in range(0, n):
        kruskal(g)
        x+=1
    ft = time.time()
    return ft - st

print(n_times(1000))
print(n_times_(1000))
print(n_timesl(1000))
print(n_times_l(1000))
print(kruskal_dict(Gd))
